package com.dataverse.demo.service;

import com.dataverse.demo.domain.User;
import com.dataverse.demo.form.RegisterForm;
import com.dataverse.demo.mapper.RegisterFormToUser;
import com.dataverse.demo.mapper.UserToUserModel;
import com.dataverse.demo.model.UserModel;

import com.dataverse.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserToUserModel mapper;

    @Autowired
    private RegisterFormToUser mapper2;


    @Override
    public void registerUser(RegisterForm registerForm) {
        User user = mapper2.mapToUser(registerForm);
        userRepository.save(user);
    }


     @Override
    public UserModel findByEmail(String email) {
                 User user = userRepository.findByEmail(email);
                 return mapper.mapToUserModel(user);
    }

}
