package com.dataverse.demo.service;

import com.dataverse.demo.form.RegisterForm;
import com.dataverse.demo.model.UserModel;

public interface UserService {

    void registerUser(RegisterForm registerForm);

    UserModel findByEmail(String email);

}
