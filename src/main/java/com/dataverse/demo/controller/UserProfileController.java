package com.dataverse.demo.controller;

import com.dataverse.demo.model.UserModel;
import com.dataverse.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class UserProfileController {

    private static final String USER="user";

    @Autowired
    private UserService userService;

   @GetMapping({"userProfile"})
    public String user(Model model) {

       String email = SecurityContextHolder.getContext().getAuthentication().getName();
       UserModel user = userService.findByEmail(email);
       model.addAttribute(USER, user);

        return "/userProfile";
    }

}

