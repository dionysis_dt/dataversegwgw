package com.dataverse.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {
    @GetMapping({"home"})
    public String first() {
        return "/home";
    }
}
